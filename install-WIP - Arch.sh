#!/bin/bash

# Agrega todos los paquetes necesarios aquí
 #Esenciales
packages="alacritty bspwm feh firefox leafpad lightdm lightdm-gtk-greeter nano numlockx picom polybar rofi sxhkd xorg xorg-xinit thunar gvfs vlc file-roller flameshot gpicview lxappearance nitrogen lxsession-gtk3 network-manager-applet"

 #Fonts
packages+="noto-fonts noto-fonts-emoji nerd-fonts"

 #Audio
packages+="pulseaudio pavucontrol pulseaudio-bluetooth alsa-plugins pulseaudio-alsa pulseaudio-jack"

 # Gparted + Plugins
packages+="gparted ntfs-3g dosfstools"

 # Bluetooth
packages+="blueman blueberry bluez"

 # Programas opcionales
packages+="htop neofetch libreoffice nmap keepassxc telegram-desktop audacity obs-studio"

#packages+=""

# alacritty = emulador de terminal
# bspwm = Window Manager
# feh = visor de imagenes y wallpaper
# firefox = navegador
# htop = visor de procesos
# leafpad = editor de texto
# lightdm = gestor de inicio
# lightdm-gtk-greeter = tema para lightdm
# nano = editor de texto en consola
# neofetch = informacion del sistema
# numlockx = establece el estado de Bloq Num
# picom = compositor de escritorio (Transparencias)
# polybar = barra de estado
# rofi = lanzador de apps
# sxhkd = gestor de atajos
# xorg = motor grafico completo
# xorg-xinit = permite "startx"
# thunar = gestor de archivos
# gvfs = gnome virtual file system
# vlc = reproductor de videos, gifs, etc
# file_roller = archivador de comprimidos
# libreoffiice = ofimatica completa
# nmap = utilidades para redes
# keepassxc = gestor de contraseñas
# flameshot = screenshots
# telegram-desktop = mensajeria
# audacity = grabador de sonido
# obs-studio = grabador de video
# gpicview = visor de imagenes
# mtpfs = detecta dispositivos android conectados por usb

# Terminar la lista

# Función para hacer un backup de un archivo o carpeta
backup() {
  local path="$1"
  local backup="$path.BACKUP"

  if [ -e "$path" ] || [ -d "$path" ]; then
    mv "$path" "$backup"
    echo "Backup creado para $path: $backup"
  fi
}

# Backup de archivos
backup $HOME/.bashrc
backup $HOME/.xinitrc
backup $HOME/.xprofile

# Verificar existencia de carpetas y hacer backup si es necesario
backup $HOME/.config/bspwm
backup $HOME/.config/polybar
backup $HOME/.config/sxhkd
backup $HOME/.config/alacritty
backup $HOME/.config/neofetch

# Verificar existencia de carpetas y ejecutar stow si es necesario
cd $HOME/.katDotFiles
stow .
cd

# Dar permisos de ejecución al archivo bspwmrc
chmod +x $HOME/.config/bspwm/bspwmrc

# Instalar paquetes
sudo pacman -S $packages