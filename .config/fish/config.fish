if status is-interactive
    set cblue "#87AFFF"
    set cgreen "#AFFF5F"
    # Commands to run in interactive sessions can go here
    function fish_prompt
     set_color -o $cblue
     echo -n (whoami)
     set_color -o $cgreen
     echo -n ' @ '
     set_color -o $cblue
     echo -n (hostnamectl hostname)
     set_color -o $cgreen
     echo -n ' | '
     set_color -o $cblue
     echo -n (prompt_pwd)
     set_color -o $cgreen
     echo -n ' > '
     set_color normal
    end
set -g fish_greeting
end
