#!/bin/sh
#xrandr --output DP-1 --mode 1360x768 --pos 1920x0 --rotate normal --output DP-2 --off --output DP-3 --off --output HDMI-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output DVI-D-1 --off

#xrandr --newmode "1920x1080_75.00" 220.64  1920 2056 2264 2608  1080 1081 1084 1128  -HSync +Vsync
#xrandr --output DP-1 --mode 1360x768 --pos 1920x0 --rotate normal --output DP-2 --off --output DP-3 --off --output HDMI-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output DVI-D-1 --off

# Define un nuevo modo con 75Hz para 1920x1080
xrandr --newmode "1920x1080_75.00" 220.64 1920 2056 2264 2608 1080 1081 1084 1128 -HSync +Vsync

# Agrega el nuevo modo al conector correspondiente
xrandr --addmode HDMI-1 1920x1080_75.00

# Aplica la configuración con la nueva frecuencia de actualización
xrandr --output DP-1 --mode 1360x768 --pos 1920x0 --rotate normal --output DP-2 --off --output DP-3 --off --output HDMI-1 --primary --mode 1920x1080_75.00 --pos 0x0 --rotate normal --output DVI-D-1 --off
